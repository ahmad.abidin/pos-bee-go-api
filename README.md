# TUTORIAL to create POS (Point Of Sale) API app with Bee Go : Golang Framework
e-mail : ahmad.abidin.init@gmail.com<br><br>
![Bee Go Logo](https://beego.me/static/img/beego_purple.png)<br>
Source Image : [beego.me/static/img/beego_purple.png](https://beego.me/static/img/beego_purple.png)<br>
BeeGo Docs : [beego.me/docs](https://beego.me/docs)

**TABLE OF CONTENTS**
---
- [PREPERATIONS](#preperations) 
    - [Install Golang](#install-golang)
    - [Create first project Bee Go API](#create-first-project-bee-go-api)
    - [Swagger](#swagger)
    - [Vendoring](#vendoring)
---
# PREPERATIONS
## Install Golang
- Download installer golang here [golang.org/dl/](https://golang.org/dl/) and choose according to your operating system 
## Create first project Bee Go API
- to create bee go API application, type this command `bee api <project_name>` in your terminal:<br>
    in this case the command will be:
    ```shell
    $ bee api pos-bee-go-api
    ```
- Below is the generated project structure of a new API application:
    ```shell
    pos-bee-go-api
    ├── conf
    │   └── app.conf
    ├── controllers
    │   └── object.go
    │   └── user.go
    ├── models
    │   └── object.go
    │   └── user.go
    ├── routers
    │   └── router.go
    ├── tests
    │   └── default_test.go
    └── main.go
    ```
## Swagger
Swagger is needed to craete API documentation of our application
- to generate swagger, type this command in your terminal:
    ```shell
    $ bee run -downdoc=true -gendoc=true
    ```
- go to [localhost:8080/swagger/](http://localhost:8080/swagger/) to test API
## Vendoring
Vendoring is useful for centralizing packages or dependencies or 3rd party libraries used in specific projects.<br>

You can add dependencies to the ***vendor*** folder by ***copy-paste***, but which clearly is not practical (and ***can cause problems***). A more correct way is to use package management tools.<br>

The package management tools used in this project is [dep](https://golang.github.io/dep/).
- to install [dep](https://golang.github.io/dep/docs/installation.html), type this command in your terminal:
    ```shell
    $ go get -u github.com/golang/dep/cmd/dep
    ```
- go to poject folder `~/<project_path>/<project_folder_name>`
- in the project folder, type this command in your terminal:
    ```shell
    $ dep init
    $ dep ensure
    ```
    Run the `dep init` command to initialize the project so that it is marked as a project using the [dep](https://golang.github.io/dep/) package manager.<br>
    
    Execution of the `dep init` command will automatically be followed by `dep ensure` execution. The `dep ensure` command itself is the most important command in [dep](https://golang.github.io/dep/), the point is to synchronize and ensure that all 3rd party libraries used in the metadata project are properly and correctly mapped out in Gopkg.*, And the 3rd party source code itself is in the vendor.<br>

    note : `so the command 'dep ensure' no longer needed if command 'dep init' has been executed`, but if you want to make sure that all 3rd party libraries has been added into vendor folder, u can type the command `dep ensure`.
- there the `vendor` folder, `Gopkg.lock` and `Gopkg.toml` file will be added in project folder:
    ```shell
    pos-bee-go-api-api
    ├── conf
    │   └── app.conf
    ├── controllers
    │   └── object.go
    │   └── user.go
    ├── models
    │   └── object.go
    │   └── user.go
    ├── routers
    │   └── router.go
    ├── tests
    │   └── default_test.go
    ├── vendor
    │   └── ... (third party folders)
    ├── Gopkg.lock
    ├── Gopkg.toml
    └── main.go
    ```